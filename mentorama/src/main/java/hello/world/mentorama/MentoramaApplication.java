package hello.world.mentorama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MentoramaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MentoramaApplication.class, args);
	}

}
